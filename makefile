all:
	ocamlc -I build -c src/mli/points.mli -o build/points.cmi
	ocamlc -I build -c src/points.ml -o build/points.cmo
	ocamlc -I build -c src/mli/triangles.mli -o build/triangles.cmi
	ocamlc -I build -c src/triangles.ml -o build/triangles.cmo
	ocamlc -I build -c src/mli/display.mli -o build/display.cmi
	ocamlc -I build -c src/display.ml -o build/display.cmo
	ocamlc -I build -c src/mli/functions.mli -o build/functions.cmi
	ocamlc -I build -c src/functions.ml -o build/functions.cmo
	ocamlc -I build -c src/demoDelaunay.ml -o build/demoDelaunay.cmo
	ocamlc -I build -c src/demoDelaunaySBS.ml -o build/demoDelaunaySBS.cmo
	ocamlc -I build -c src/demoPoints.ml -o build/demoPoints.cmo
	ocamlc -I build -c src/demoDelaunayShuffle.ml -o build/demoDelaunayShuffle.cmo
	ocamlc -I build -c src/tests.ml -o build/tests.cmo
	ocamlc -I build unix.cma graphics.cma build/points.cmo build/triangles.cmo build/display.cmo build/functions.cmo build/demoDelaunay.cmo -o bin/demoDelaunay
	ocamlc -I build unix.cma graphics.cma build/points.cmo build/triangles.cmo build/display.cmo build/functions.cmo build/demoDelaunaySBS.cmo -o bin/demoDelaunaySBS
	ocamlc -I build unix.cma graphics.cma build/points.cmo build/triangles.cmo build/display.cmo build/functions.cmo build/demoPoints.cmo -o bin/demoPoints
	ocamlc -I build unix.cma graphics.cma build/points.cmo build/triangles.cmo build/display.cmo build/functions.cmo build/demoDelaunayShuffle.cmo -o bin/demoDelaunayShuffle
	ocamlc -I build unix.cma graphics.cma build/points.cmo build/triangles.cmo build/display.cmo build/functions.cmo build/tests.cmo -o bin/tests
	ocamlc -I build -c src/demoDynamic.ml -o build/demoDynamic.cmo
	ocamlc -I build unix.cma graphics.cma build/points.cmo build/triangles.cmo build/display.cmo build/functions.cmo build/demoDynamic.cmo -o bin/demoDynamic



