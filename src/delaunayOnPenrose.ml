open Points
open Triangles

let rec insere point points =
  if pt_is_empty points then pt_cons point (pt_empty())
  else if (equals point (pt_car points)) then
    points
  else
    pt_cons (pt_car points) (insere point (pt_cdr points))
          
let rec pt_conc_without_double pts1 pts2 =
  if pt_is_empty pts1 then pts2
  else
    pt_conc_without_double (pt_cdr pts1) (insere (pt_car pts1) pts2)

let list_of_array v =
  let n = Array.length v in
  let rec aux n =
    if n = 0 then pt_empty()
    else
      pt_cons (v.(n-1)) (aux (n-1))in
  aux n

let rec divide generation points types =
  let phi = (1.+.sqrt(5.))/.2. in
  if generation = 0 then
    list_of_array points
  else
    begin
      if types = "Obtuse" then
        begin
          let p0 = points.(0)
          and p1 = points.(1)
          and p2 = points.(2) in
          let pt = point_of ((1. /. phi) *. (get_x p2) +.
                               (1. -. 1./.phi) *. (get_x p0))
                            ((1. /. phi) *. (get_y p2) +.
                               (1. -. 1./.phi) *. (get_y p0)) in
          pt_conc_without_double (divide (generation - 1)
                                         [|pt ; p0; p1|] "Acute")
                                 (divide (generation - 1)
                                         [|p2; pt; p1|] "Obtuse")
        end
      else
        begin
          let p0 = points.(0)
          and p1 = points.(1)
          and p2 = points.(2) in
          let pt1' = point_of ((1. /. phi) *. (get_x p0)  +.
                                 (1. -. 1./.phi) *. (get_x p1))
                              ((1. /. phi) *. (get_y p0)  +.
                                 (1. -. 1./.phi) *. (get_y p1)) in
          let pt2' = point_of (1. /. phi *. (get_x p1) +.
                                 (1. -. 1./.phi) *. (get_x p2))
                              (1. /. phi *. (get_y p1) +.
                                 (1. -. 1./.phi) *. (get_y p2)) in
          pt_conc_without_double
            (divide (generation - 1) [|pt1'; p2; p0|] "Acute")
            (pt_conc_without_double
               (divide (generation - 1) [|pt2'; p2; pt1'|] "Acute")
               (divide (generation - 1) [|p1; pt2'; pt1'|] "Obtuse"))
        end
    end
  
let next p0 p1 a =
  let pi = 4.0 *. atan 1.0 in
  let x0 = get_x p0
  and y0 = get_y p0
  and x1 = get_x p1
  and y1 = get_y p1 in
  let l = sqrt((x0-.x1)**2.+.(y0-.y1)**2.) in
  if y0 > y1 then
    let b = acos((x0-.x1)/.l) in
    point_of (x1 +. (cos (b+.a*.pi/.5.)) *.l) (y1 +. (sin (b+.a*.pi/.5.)) *. l)
  else 
    let b = -.acos((x0-.x1)/.l) in
    point_of (x1 +. (cos (b+.a*.pi/.5.)) *.l) (y1 +. (sin (b+.a*.pi/.5.)) *. l)
    


                                    
                
          
                                              
