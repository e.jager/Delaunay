open Points
open Display
open Graphics
open Functions
open DelaunayOnPenrose
open Unix

let () =
  open_graph " 800x600-0+0";
  let phi = (1.+.sqrt(5.))/.2. in
  let height = 600.
  and width = 800. in
  let d = 200. in
  let x0 = width /. 2. -. sqrt ((phi*.d)**2.-. d**2./.4.)
  and y0 = height/.2. -. d/.2.
  and x1 = width/.2.
  and y1 = height/.2. in
  let p0 = point_of x0 y0
  and p1 = point_of x1 y1 in
  let p2 = next p0 p1 (-1.) in

  draw_triangles (delaunay (divide 4 [|p0; p1; p2|] "Acute") 800 600);
  Unix.sleep 5
  
