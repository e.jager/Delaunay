open Graphics
open Functions
open Display
open Unix


(* This is a demo about delaunay function *)
(* Delaunay is called, and the returned triangles are drawn *)
let () = 
  
  Random.self_init(); 
  let points = random 20 800 600 in
    open_graph " 820x620-0+0";
    draw_triangles (delaunay points 800 600);
    draw_points points;
    Unix.sleep 1
