open Functions
open Display
open Graphics

(* This is a demo about delaunay_step_by_step *)
(* The screen is refreshed each times a point is added to the structure *)
let () = 
  let points = random 20 800 600 in
    Random.init 1;
    open_graph " 820x620-0+0";
    delaunay_step_by_step points 800 600
