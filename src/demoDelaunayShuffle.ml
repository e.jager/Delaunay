open Functions
open Display
open Points
open Graphics
open Unix

(*This is a demo to show that the result of delaunay doesn't depend on the point list order *)
let () =
  let points = random 20 820 620 in
  open_graph " 800x600-0+0";
  draw_triangles (delaunay points 800 600);
  Unix.sleep 2;
  draw_triangles (delaunay (pt_rev points) 800 600);
  Unix.sleep 2
  
  
