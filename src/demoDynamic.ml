open Points
open Graphics
open Functions
open Display
open Unix

(* This is a dynamic delaunay *)
(* The user can move, add or delete a point in real time *)


(* Defines the exception used to quit the main while *)
exception Quit

(* Defines the type used to follow a focused point *)
type underMouse = {mutable focused: bool; mutable point: point}

(* FocusedPoint.focused is true if the point focusedPoint.point is followed *)
let focusedPoint = {focused = false; point = point_of 0. 0.}

let point_size = get_point_size ()
let points = random 20 800 600

(* This function is called each time the user click down a mouse button *)
(* It aims to follow the point under the cursor or define a new point *)
let rec compute_focused_point mouse_x mouse_y remaining_points =
  (* If no existing point is under the cursor *)
  if (pt_is_empty remaining_points) then
    begin
      let point = ( point_of (float_of_int (mouse_x - 10 )) (float_of_int (mouse_y - 10))) in
      focusedPoint.focused <- true;
      focusedPoint.point <- point;
      pt_add_point points point;
	
    end
  else
    let point = pt_car remaining_points in
    let a = int_of_float (get_x point) in
    let b = int_of_float (get_y point) in
    let dx = a - mouse_x + 10 in
    let dy = b - mouse_y + 10 in
    (* If point is under the cursor *)
    if (dx*dx + dy*dy <= point_size*point_size) then
      begin
	focusedPoint.focused <- true;
	focusedPoint.point <- point;
      end
    else compute_focused_point mouse_x mouse_y (pt_cdr remaining_points)
      
let () = 
  
   
  Random.self_init();
  
  (* This variable is true if the mouse button was down *)
  let down = ref false in
  
  open_graph " 820x620-0+0";
  auto_synchronize false;


  (* The first draw *)
  draw_points points;
  draw_triangles (delaunay points 800 600);
  synchronize ();

  while (true) do
    
    (* If a point is being modified *)
    if focusedPoint.focused then
      begin
	clear_screen 800 600;
        draw_points points;  
	draw_point focusedPoint.point red;
	draw_triangles (delaunay points 800 600);
        synchronize ();
      end;

    (* This wait untill an interface event is raised *)
    let status =  wait_next_event [Button_down; Button_up; Key_pressed; Mouse_motion] in
    if not (!down = status.button) then
      (* Something has changed with the mouse buttons *)
      begin
	down := status.button;
	if !down then (* The user has push down a mouse button *)
	  begin
	    compute_focused_point (status.mouse_x) (status.mouse_y) points
	  end
	else (* The user has released a mouse button *)
	  begin
	    focusedPoint.focused <- false;	      
	    draw_point focusedPoint.point black;
	  end
      end

    else (* Nothing has changed with the mouse buttons *)
      begin
	(* A keyboard key has been pushed *)
        if status.keypressed then 
	  begin
	    (* If it's the 'd' key -> used to delete a point *)
	    if status.key = (char_of_int 100) && focusedPoint.focused  then
	      (pt_del_point points focusedPoint.point)
	    (* It's not 'd', the process terminates *)
	    else
	      raise Quit
	  end
	else
	  begin (* The mouse cursor has moved *)
	    
	    
	    if (focusedPoint.focused) && status.button then
	      begin
		set_x (focusedPoint.point) (float_of_int status.mouse_x);
		set_y (focusedPoint.point) (float_of_int status.mouse_y);
	      end
	  end
      end
  done;
