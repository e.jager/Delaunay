open Graphics
open Functions
open Display
open Unix


(* This is a demo to show that the random point generation works fine *)
let () =
  Random.self_init();
  open_graph " 820x620-0+0";
  let points = random 20 800 600 in
    draw_points points;
    Unix.sleep 1;
