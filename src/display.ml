open Graphics
open Unix
open Points
open Triangles

(* This librairy is used to draw on the screen *)

let point_size = 5
               
let get_point_size () = point_size
  
let draw_point point color =
  set_color color;
  draw_circle (int_of_float(get_x point) +10)
    (int_of_float(get_y point) +10)
    point_size;
  set_color black
   
let rec draw_points points =
  if not (pt_is_empty points) then
    let point = pt_car points in
    draw_point point black;
    draw_points (pt_cdr points)


let draw_triangles triangles =
    let rec aux tr=
    if tr_is_empty tr then ()
    else begin
      let triangle = tr_car tr in
      let p1 = get_p1 triangle
      and p2 = get_p2 triangle
      and p3 = get_p3 triangle in
      draw_poly [|(int_of_float (get_x p1) + 10, int_of_float (get_y p1) + 10);
		  (int_of_float (get_x p2)+10, int_of_float (get_y p2) + 10);
		  (int_of_float (get_x p3)+10, int_of_float (get_y p3) + 10)|];
      aux (tr_cdr tr)
	end
  in
 aux triangles 

let clear_screen max_x max_y = 
	set_color white;
	fill_poly
	  [|
	    (0, max_y + 20);
	    (max_x +20, max_y + 20);
	    (max_x + 20, 0);
	    (0, 0)
	  |];
	set_color black

