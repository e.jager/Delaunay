open Points
open Triangles
open Display

(* This librairy contains the core functions of delaunay algorithms *)

(* Computes the determinant of the given nxn matrix *)
(* TODO: in place ? *)
let rec det matrix =
  let n = Array.length matrix in
  if n = 1 then
    matrix.(0).(0)
  else begin
    let result = ref 0. in
    for i = 0 to n-1 do
      let minor = Array.make_matrix (n-1) (n-1) 0. in
      for j = 1 to n-1 do
        for k = 0 to i-1 do
	  minor.(j-1).(k) <- matrix.(j).(k)
        done;
        for k = i+1 to n-1 do
	  minor.(j-1).(k-1) <- matrix.(j).(k)
        done;
      done;
      if i mod 2 = 0 then
        result := (!result) +. (matrix.(0).(i) *. (det minor))
      else
        result := (!result) -. (matrix.(0).(i) *. (det minor))
    done;
    !result
    end
	
  

let random nb max_x max_y =
  let rec rand_aux n l =
    if n = 0 then l
    else
      let x = Random.float (float_of_int max_x)
      and y = Random.float (float_of_int max_y) in
      rand_aux (n-1) (pt_cons (point_of x y) l) in
    rand_aux nb (pt_empty())


let ccw a b c =
  det [|[|(get_x b)-.(get_x a); (get_x c)-.(get_x a)|];
       [|(get_y b)-.(get_y a); (get_y c)-.(get_y a)|]|]
  > 0.


(* this function is used to tell if a point is inside 
the circle surrounding the given triangle*)
let in_circle triangle point =
  let a = get_p1 triangle in
  let b = get_p2 triangle in
  let c = get_p3 triangle in
  let ax, ay = get_x a, get_y a in
  let bx, by = get_x b, get_y b in
  let cx, cy = get_x c, get_y c in
  let d = point in
  let dx, dy = get_x d, get_y d in
  let direct = ccw a b c in
  let discriminant = det
    [|
      [| ax; ay; dot_product a a; 1.|] ;
      [| bx; by; dot_product b b; 1.|] ;
      [| cx; cy; dot_product c c; 1.|] ;
      [| dx; dy; dot_product d d; 1.|] ;
    |]
  in   
  if discriminant > 0. then
    direct
  else
    not direct

(* I act as if the union of all the triangles
of the list `Triangles` make a convexe polygone. *)

let border triangles =
  
(* aux make thr list of vertices of the shape of 
the union of triangles*)
  let rec aux trs =
    if tr_is_empty trs then []
    else
      begin
        let triangle = tr_car trs in
        inser_end (get_p1 triangle, get_p2 triangle)
                  (inser_end (get_p2 triangle, get_p3 triangle)
                             (inser_end (get_p1 triangle, get_p3 triangle)
                                        (aux (tr_cdr trs))));
      end
    
(* inser_end take a couple c of point and a list of
couples l and add c in l if c or the reverse of c is
not in l. If it is, it delete the two couples. *)
  and inser_end (pt1, pt2) l =
    if l = [] then
      [(pt1, pt2)]
    else
      begin
        let (pt1', pt2') = List.hd l in
        if (equals pt1 pt1' && equals pt2 pt2') ||
             (equals pt1 pt2' && equals pt1 pt1') then
          List.tl l
        else
          (pt1', pt2')::(inser_end (pt1, pt2) (List.tl l))
      end in
  aux triangles

    
let add_point triangles point =
  let rec triangle_circle tr =
    if (tr_is_empty tr) then tr_empty (),tr_empty ()
    else let tr1,tr2 = triangle_circle (tr_cdr tr) in
	 if (in_circle (tr_car tr) point) then (tr_cons (tr_car tr) tr1),tr2
	 else tr1, (tr_cons (tr_car tr) tr2)
  in
  let rec new_triangle points = match points with
    |[]-> tr_empty ()
    |h::t-> let a,b = h in
            tr_cons (triangle_of a b point) (new_triangle t)
  in
  let tr1,tr2 = triangle_circle triangles in
  let tr3 =  new_triangle (border tr1) in
  tr_conc tr3 tr2


let delaunay points max_x max_y =
  let float_max_x = float_of_int max_x in
  let float_max_y = float_of_int max_y in
  let top_left = point_of 0. float_max_y in
  let top_right = point_of float_max_x float_max_y in
  let bottom_left = point_of 0. 0. in
  let bottom_right = point_of float_max_x 0. in
  let top_right_triangle =
    triangle_of top_left top_right bottom_right in
  let bottom_left_triangle =
    triangle_of top_left bottom_right bottom_left in
  let triangles =
    tr_cons
      bottom_left_triangle
      (tr_cons top_right_triangle (tr_empty ())) in
  let rec delaunay_aux triangles points =
    if (pt_is_empty points) then
      triangles
    else
      begin
	let next_triangles = add_point triangles (pt_car points) in
	delaunay_aux next_triangles (pt_cdr points)
      end
  in
  
  delaunay_aux triangles points
 
(* TODO: factorisation ? *)
let delaunay_step_by_step points max_x max_y =
  let float_max_x = float_of_int max_x in
  let float_max_y = float_of_int max_y in
  let top_left = point_of 0. float_max_y in
  let top_right = point_of float_max_x float_max_y in
  let bottom_left = point_of 0. 0. in
  let bottom_right = point_of float_max_x 0. in
  let top_right_triangle =
    triangle_of top_left top_right bottom_right in
  let bottom_left_triangle =
    triangle_of top_left bottom_right bottom_left in
  let triangles =
    tr_cons
      bottom_left_triangle
      (tr_cons top_right_triangle (tr_empty ())) in
  let rec delaunay_aux triangles points =
    if not (pt_is_empty points) then
      begin
	let next_triangles = add_point triangles (pt_car points) in
	clear_screen max_x max_y;
	draw_triangles next_triangles;
	draw_points points;
	Unix.sleep 5;
	delaunay_aux next_triangles (pt_cdr points)
      end
  in
  
  delaunay_aux triangles points

