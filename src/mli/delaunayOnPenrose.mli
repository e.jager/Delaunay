val insere : Points.point -> Points.point_set -> Points.point_set
val pt_conc_without_double :
  Points.point_set -> Points.point_set -> Points.point_set
val list_of_array : 'a array -> 'a list ref
val divide : int -> Points.point array -> string -> Points.point_set
val next : Points.point -> Points.point -> float -> Points.point
