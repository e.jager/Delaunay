val point_size : int
val draw_points : Points.point list -> unit
val draw_triangles : Triangles.triangle list -> unit
val clear_screen : int -> int -> unit
