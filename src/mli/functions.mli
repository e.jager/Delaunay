val det : float array array -> float
val random : int -> int -> int -> Points.point list
val ccw : Points.point -> Points.point -> Points.point -> bool
val in_circle : Triangles.triangle -> Points.point -> bool
val border : Triangles.triangle list -> (Points.point * Points.point) list
val add_point :
  Triangles.triangle list -> Points.point -> Triangles.triangle list
val delaunay : Points.point list -> int -> int -> Triangles.triangle list
val delaunay_step_by_step : Points.point list -> int -> int -> unit
