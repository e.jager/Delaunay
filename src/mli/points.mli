type point = { x : float; y : float; }
type point_set = point list
val point_of : float -> float -> point
val get_x : point -> float
val get_y : point -> float
val dot_product : point -> point -> float
val equals : point -> point -> bool
val pt_car : 'a list -> 'a
val pt_cdr : 'a list -> 'a list
val pt_empty : unit -> 'a list
val pt_cons : 'a -> 'a list -> 'a list
val pt_is_empty : 'a list -> bool
val pt_rev : 'a list -> 'a list
