type triangle = { p1 : Points.point; p2 : Points.point; p3 : Points.point; }
type triangle_set = triangle list
val triangle_of : Points.point -> Points.point -> Points.point -> triangle
val get_p1 : triangle -> Points.point
val get_p2 : triangle -> Points.point
val get_p3 : triangle -> Points.point
val is_summit_of : Points.point -> triangle -> bool
val tr_equals : triangle -> triangle -> bool
val tr_car : 'a list -> 'a
val tr_cdr : 'a list -> 'a list
val tr_empty : unit -> 'a list
val tr_is_empty : 'a list -> bool
val tr_cons : 'a -> 'a list -> 'a list
val tr_conc : 'a list -> 'a list -> 'a list
