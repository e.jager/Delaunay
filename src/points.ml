type point = {mutable x: float;mutable  y: float}

type point_set = point list ref

let point_of a b = {x = a ; y = b}

let get_x point = point.x

let get_y point = point.y

let set_x point a = point.x <- a

let set_y point b = point.y <- b


(* computes the dot product of two *)
(* points seen as two vectors *)
(* TODO: infix ? *)
let dot_product point1 point2 =
    ((get_x point1) *. (get_x point2))
    +. ((get_y point1) *. (get_y point2))

let equals pt1 pt2 =
    get_x pt1 = get_x pt2 &&
      get_y pt1 = get_y pt2
                
let pt_car points = match (!points) with
    | [] -> failwith "pt_car"
    | a :: q -> a

let pt_cdr points = match !points with
    | [] -> failwith "pt_cdr"
    | a :: q -> ref q

let pt_empty () = ref []

let pt_cons point points = ref (point::(!points))
                        
let pt_is_empty (points:point_set) = (!points) = !(pt_empty())

let pt_add_point points point =
  points := point::(!points)

let pt_del_point points point =
  let rec aux points2 point = match points2 with
  |[] -> []
  |a::next -> if (equals a point) then aux next point
    else a::(aux next point)
  in
  points := (aux !points point)

let pt_rev l =
  let rec aux acc l =
    if pt_is_empty l then acc
    else
      aux (pt_cons (pt_car l) acc) (pt_cdr l) in
  aux (pt_empty()) l
