open Points
open Triangles
open Functions
open Display

       (* Tests Points *)

let () =

  print_newline ();
  print_string "=======[ Tests Points ]=======";
  print_newline ();
  print_newline ();
  
  print_string "test point_of : ";
  if (point_of 1. 2. = {x = 1.; y = 2.}) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string  "test get_x : ";
  if (get_x (point_of 1. 2.) = 1.) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string  "test get_y : ";
  if (get_y (point_of 1. 2.) = 2.) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string  "test dot_poduct : ";
  if (dot_product (point_of 1. 2.) (point_of 4. 2.) = 8.) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string  "equals 1 : ";
  if (equals (point_of 1. 2.) (point_of 1. 2.)) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string  "equals 2 : ";
  if not (equals (point_of 1. 2.) (point_of 2. 2.)) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();

  print_string "pt_empty : not tested";
  print_newline ();

  print_string "pt_cons : not tested";
  print_newline ();
  
  print_string  "pt_car : ";
  if (equals (pt_car (pt_cons (point_of 1. 2.) (pt_empty())))
             (point_of 1. 2.)) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string  "pt_cdr : not tested";
  print_newline ();

  print_string  "test pt_is_empty : ";
  if pt_is_empty (pt_empty()) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string  "test pt_rev : ";
  if (equals (pt_car (pt_rev (pt_cons (point_of 4. 2.)
                           (pt_cons (point_of 1. 2.) (pt_empty())))))
            (point_of 1. 2.)) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();

  let _ = read_line() in ();
  

  (* Tests Triangles *)

  let a = point_of 1. 1.
  and b = point_of 3. 1.
  and c = point_of 2. 3. in

  print_newline ();
  print_string "=======[ Tests Triangles ]=======";
  print_newline ();
  print_newline ();
  
  print_string "test triangle_of : ";
  if (triangle_of a b c  = ({p1 = a; p2 = b; p3 = c})) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string  "test get_p1 : ";
  if (equals (get_p1 (triangle_of a b c)) a) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string  "test get_p2 : ";
  if (equals (get_p2 (triangle_of a b c)) b) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string  "test get_p3 : ";
  if (equals (get_p3 (triangle_of a b c)) c) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string  "test is_summit_of 1 : ";
  if (is_summit_of a (triangle_of a b c)) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string  "test is_summit_of 2 : ";
  if not (is_summit_of (point_of 3. 3.) (triangle_of a b c)) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string  "test tr_equals 1 : ";
  if (tr_equals (triangle_of a b c) (triangle_of a b c)) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string  "test tr_equals 2 : ";
  if (tr_equals (triangle_of a b c) (triangle_of a c b)) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string  "test tr_equals 3 : ";
  if not (tr_equals (triangle_of a b c) (triangle_of a b (point_of 3. 3.))) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string "tr_empty : not tested";
  print_newline ();

  print_string "tr_cons : not tested";
  print_newline ();

  print_string "tr_cdr : not tested";
  print_newline ();
  
  print_string "tr_conc : not tested";
  print_newline ();
  
  let _ = read_line() in ();

  (* Tests Functions *)
  
  print_newline ();
  print_string "=======[ Tests Functions ]=======";
  print_newline ();
  print_newline ();

  print_string  "test det : ";
  if det [|
         [|2.; 5.; 6.|];
         [|5.; 1.; 3.|];
         [|6.; 3.; 1.|]
       |] = 103.
  then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string "test ccw 1 : ";
  if ccw a b c then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string "test ccw 2 : ";
  if not (ccw a c b) then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string "test in_circle 1 : ";
  if  not (in_circle (triangle_of a b c) (point_of 3. 3.))  then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  print_string "test in_circle 2 : ";
  if  (in_circle (triangle_of a b c) (point_of 2. 1.))  then
    print_string "success"
  else
    print_string "fail";
  print_newline ();
  
  
