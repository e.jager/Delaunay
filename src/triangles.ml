open Points

type triangle = {p1: point; p2: point; p3: point}
    
type triangle_set = triangle list
   
let triangle_of point1 point2 point3 =
    ({p1 = point1; p2 = point2; p3 = point3}:triangle)

let get_p1 triangle = triangle.p1
                   
let get_p2 triangle = triangle.p2
                   
let get_p3 triangle = triangle.p3

let is_summit_of point triangle =
  equals (get_p1 triangle) point ||
    equals (get_p2 triangle) point ||
      equals (get_p3 triangle) point

let tr_equals tr1 tr2 =
  is_summit_of (get_p1 tr1) tr2 &&
    is_summit_of (get_p2 tr1) tr2 &&
      is_summit_of (get_p3 tr1) tr2

let tr_car triangles = match triangles with
    | [] -> failwith "tr_car"
    | a :: q -> a

let tr_cdr triangles = match triangles with
    | [] -> failwith "tr_cdr"
    | a :: q -> q

let tr_empty () = []
            
let tr_is_empty triangles =
    triangles = tr_empty()

let tr_cons triangle triangles =
    triangle :: triangles

let rec tr_conc triangles1 triangles2 =
    match triangles1 with
      |[] -> triangles2
      |h::t -> tr_conc t (h::triangles2)


